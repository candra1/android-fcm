package co.g2academy.android_fcm3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView token_tv;
    EditText token_et;
    String token="";
    Button token_bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewByIdGroup();
        onClickGroup();
    }
    private void findViewByIdGroup(){
        token_tv = (TextView) findViewById(R.id.token_tv);
        token_et = (EditText) findViewById(R.id.token_et);
        token_bt = (Button) findViewById(R.id.token_bt);
    }
    private void loadToken() {
        SharedPreferences sharedPref = getSharedPreferences("co.g2academy.android_fcm", Context.MODE_PRIVATE);
        token = sharedPref.getString("co.g2academy.android_fcm.token", "-");
        token_et.setText(token);
    }
    void onClickGroup() {
        token_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadToken();
            }
        });
    }
}
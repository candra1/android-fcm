package co.g2academy.android_fcm3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

public class MessageActivity extends AppCompatActivity {
    EditText titleEt, messageEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        findViewByIdGroup();
        loadData();
    }

    private void findViewByIdGroup(){
        titleEt = (EditText) findViewById(R.id.title_et);
        messageEt = (EditText) findViewById(R.id.message_et);
    }

    private void loadData(){
        Bundle bundle = getIntent().getExtras();
        titleEt.setText(bundle.getString("title"));
        messageEt.setText(bundle.getString("message"));
    }
}